# Phabricator Summarization

## Setup

`python -m venv venv`

`pip install -r requirements.txt`

`vi .env`

`OPENAI_API_KEY=<your key here>`

## Run

`uvicorn --reload --host 0.0.0.0 --port 8787 main:app`