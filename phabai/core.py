from langchain.chains.combine_documents.stuff import StuffDocumentsChain
from langchain.chains.llm import LLMChain
from langchain_community.document_loaders import WebBaseLoader
from langchain_openai import ChatOpenAI
from langchain_community.llms import Ollama
from langchain.prompts import PromptTemplate

from dotenv import load_dotenv

load_dotenv()

# https://phabricator.wikimedia.org/T350180

base_url = "https://phabricator.wikimedia.org/"

prompt_template = """Write a concise summary of the following:
"{text}"
CONCISE SUMMARY:"""
prompt = PromptTemplate.from_template(prompt_template)

# llm = ChatOpenAI(temperature=0, model_name="gpt-4o-mini")
llm = Ollama(model="llama3.1:8b")


def load_phab(phab_id: str) -> str:
    url = f"{base_url}{phab_id}"
    loader = WebBaseLoader(url)
    return loader.load()


def summarize(text) -> dict:
    llm_chain = LLMChain(llm=llm, prompt=prompt)
    stuff_chain = StuffDocumentsChain(llm_chain=llm_chain, document_variable_name="text")
    return stuff_chain.invoke(text)


def display(result):
    print(result['input_documents'][0].page_content)
    print(result['output_text'])


if __name__ == "__main__":
    docs = load_phab("T350180")
    res = summarize(docs)
    display(res)
