from fastapi import FastAPI
from dotenv import load_dotenv

from phabai.core import load_phab, summarize

load_dotenv()

app = FastAPI()


@app.get("/")
async def status() -> dict:
    return {"status": "ok"}


@app.get("/summarize/{phab_id}")
async def summarize_phab_ticket(phab_id: str) -> dict:
    try:
        docs = load_phab(phab_id)
        res = summarize(docs)
        print(res['input_documents'][0].page_content)
        return {"status": "ok", "summary": res['output_text']}
    except Exception as e:
        print(e)
        return {"status": "error"}
